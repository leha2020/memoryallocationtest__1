// MemoryAllocationTest1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

constexpr auto ONE_GIGABYTE = 1024 * 1024 * 1024;
constexpr auto MEGABYTES_32 = 32 * 1024;
constexpr auto ITERATIONS = 15;

#include <iostream>
#include <chrono>
#include <thread>
#include <cstdlib>

using std::cout;
using std::cerr;
using std::endl;

int main()
{
    auto bufferSize = ONE_GIGABYTE;
    auto start = std::chrono::steady_clock::now();
    for (auto i = 0; i != ITERATIONS; ++i, bufferSize += MEGABYTES_32)
    {
        volatile void* memoryBuffer = malloc(bufferSize);
        if (memoryBuffer == nullptr)
        {
            cerr << "Memory allocation error" << std::endl;
            return 1;
        }
        //std::this_thread::sleep_for(std::chrono::milliseconds(500));
        memset((void*)memoryBuffer, 0, ONE_GIGABYTE);
        free((void*)memoryBuffer);
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Time elapsed " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms";
    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
